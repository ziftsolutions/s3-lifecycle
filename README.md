s3-lifecycle
============
The role creates/delets/enable/disable a lifecycle rule for an S3 bucket

Requirements
------------
- User needs AWS secret keys

Role Variables
--------------
- s3_bucket: (required) S3 bucket to apply lifecycle to
- command: get (default), set or delete
- status: enabled (default) or disabled
- idle_days: (required for put) Number of days S3 bucket is idle (unused); default is 90 days
- expire_in_days: (required for put) Number of days rule will expire; default is 3650 days (10 years)

Dependencies
------------
None

Example Playbook
----------------
Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    s3-lifecycle.yml
    - hosts: localhost
      connection: local
      become: no
      roles:
         - s3-lifecycle

    Examples:
    - Get lifecycle rule for my_s3_bucket
    $ ansible-playbook -i "localhost," s3-lifecycle.yml \
                       -e s3_bucket=my_s3_bucket

    - Set a lifecycle rule for my_s3_bucket if idle for 90 days, to expire in 10 years, and status=enabled (note these are default values)
    $ ansible-playbook -i "localhost," s3-lifecycle.yml \
                       -e "s3_bucket=my_s3_bucket  \
                           command=set"

    - Set a lifecycle rule for my_s3_bucket if idle for 30 days, to expire in 1 year, and status=enabled
    $ ansible-playbook -i "localhost," s3-lifecycle.yml \
                       -e "s3_bucket=my_s3_bucket \
                           command=set \
                           status=enabled \
                           idle_days=30 \
                           expire_in_days=365"

    - Disable a lifecycle rule for my_s3_bucket
    $ ansible-playbook -i "localhost," s3-lifecycle.yml \
                       -e "s3_bucket=my_s3_bucket \
                           command=set \
                           status=disabled"

    - Delete a lifecycle rule for my_s3_bucket
    $ ansible-playbook -i "localhost," s3-lifecycle.yml \
                       -e "s3_bucket=my_s3_bucket \
                           command=delete"

License
-------
Zift Solutions

Author Information
------------------
Chris Fouts
